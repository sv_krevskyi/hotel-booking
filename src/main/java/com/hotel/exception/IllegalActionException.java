package com.hotel.exception;

public class IllegalActionException extends RuntimeException {

    public IllegalActionException() {
    }

    public IllegalActionException(String message) {
        super(message);
    }
}
