package com.hotel.data.dao.impl;

import com.hotel.data.entity.Client;
import com.hotel.data.dao.BaseDao;
import com.hotel.data.repository.ClientRepository;
import com.hotel.exception.DAOException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class ClientDao implements BaseDao<Client> {

    private ClientRepository clientRepository;

    @Autowired
    public ClientDao(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Override
    public Client create(Client client) {
        return this.clientRepository.save(client);
    }

    @Override
    public void create(List<Client> list) {
        this.clientRepository.saveAll(list);
    }

    @Override
    public Client get(Long id) {
        return this.clientRepository.findById(id).orElseThrow(()
                -> new DAOException(String.format("Client with id '%s' is not found", id)));
    }

    @Override
    public List<Client> getAll() {
        return (List<Client>) this.clientRepository.findAll();
    }

    @Override
    public Client update(Client client) {
        return this.clientRepository.save(client);
    }

    @Override
    public List<Client> update(List<Client> list) {
        return (List<Client>) this.clientRepository.saveAll(list);
    }

    @Override
    public void delete(Long id) {
        this.clientRepository.deleteById(id);
    }

}