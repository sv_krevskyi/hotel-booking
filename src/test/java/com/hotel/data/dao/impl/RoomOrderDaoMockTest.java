package com.hotel.data.dao.impl;

import com.hotel.data.entity.Client;
import com.hotel.data.entity.Room;
import com.hotel.data.entity.RoomOrder;
import com.hotel.data.entity.RoomType;
import com.hotel.data.entity.enums.OrderStatus;
import com.hotel.data.repository.RoomOrderRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.sql.*;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

@ActiveProfiles("test")
@SpringBootTest
class RoomOrderDaoMockTest {

    //expected
    private static final Long TEST_ID = 1L;
    private static final Client TEST_CLIENT = Client.builder().id(2L).build();
    private static final RoomType TEST_ROOM_TYPE = RoomType.builder().id(3L).build();
    private static final Room TEST_ROOM = Room.builder().id(4L).roomType(TEST_ROOM_TYPE).build();

    private static final RoomOrder TEST_ROOM_ORDER_EXPECTED = RoomOrder.builder()
            .id(TEST_ID)
            .client(TEST_CLIENT)
            .roomType(TEST_ROOM_TYPE)
            .room(TEST_ROOM)
            .checkInDate(Date.valueOf("2019-09-01"))
            .checkOutDate(Date.valueOf("2019-09-10"))
            .status(OrderStatus.WAITING)
            .build();

    private static final RoomOrder TEST_ROOM_ORDER = RoomOrder.builder()
            .checkInDate(Date.valueOf("2019-09-01"))//yyyy-[m]m-[d]d
            .checkOutDate(Date.valueOf("2019-09-10"))//yyyy-[m]m-[d]d
            .status(OrderStatus.WAITING)
            .client(TEST_CLIENT)
            .roomType(TEST_ROOM_TYPE)
            .build();

    @Autowired
    private RoomOrderDao roomOrderDao;

    @MockBean
    private RoomOrderRepository mockRoomOrderRepository;

    @Test
    void whenCreateThenReturnEntityTest(){
        roomOrderDao.create(TEST_ROOM_ORDER);
        verify(mockRoomOrderRepository).save(TEST_ROOM_ORDER);
    }

    @Test
    void whenCreateListThenReturnEntityListTest(){
        roomOrderDao.create(List.of(TEST_ROOM_ORDER));
        verify(mockRoomOrderRepository).saveAll(List.of(TEST_ROOM_ORDER));
    }

    @Test
    void whenGetThenReturnEntityTest(){
        when(mockRoomOrderRepository.findById(TEST_ID)).thenReturn(Optional.of(TEST_ROOM_ORDER_EXPECTED));
        RoomOrder actual = roomOrderDao.get(TEST_ID);
        assertEquals(TEST_ROOM_ORDER_EXPECTED, actual);
    }

    @Test
    void whenGetAllThenReturnEntityListTest(){
        when(mockRoomOrderRepository.findAll()).thenReturn(List.of(TEST_ROOM_ORDER_EXPECTED));
        List<RoomOrder> actual = roomOrderDao.getAll();
        assertEquals(List.of(TEST_ROOM_ORDER_EXPECTED), actual);
    }

    @Test
    void whenGetAllByStatusThenReturnEntityListTest(){
        when(mockRoomOrderRepository.findAllByStatus(OrderStatus.WAITING)).thenReturn(List.of(TEST_ROOM_ORDER_EXPECTED));
        List<RoomOrder> actual = roomOrderDao.getAllByStatus(OrderStatus.WAITING);
        assertEquals(List.of(TEST_ROOM_ORDER_EXPECTED), actual);
    }

    @Test
    void whenGetAllByClientThenReturnEntityListTest(){
        when(mockRoomOrderRepository.findAllByClientId(2L)).thenReturn(List.of(TEST_ROOM_ORDER_EXPECTED));
        List<RoomOrder> actual = roomOrderDao.getAllClientOrders(2L);
        assertEquals(List.of(TEST_ROOM_ORDER_EXPECTED), actual);
    }

    @Test
    void whenUpdateThenReturnEntityTest(){
        roomOrderDao.update(TEST_ROOM_ORDER);
        verify(mockRoomOrderRepository).save(TEST_ROOM_ORDER);
    }

    @Test
    void whenUpdateListThenReturnEntityListTest(){
        roomOrderDao.update(List.of(TEST_ROOM_ORDER));
        verify(mockRoomOrderRepository).saveAll(List.of(TEST_ROOM_ORDER));
    }

    @Test
    void deleteTest(){
        roomOrderDao.delete(TEST_ID);
        verify(mockRoomOrderRepository).deleteById(TEST_ID);
    }

}
