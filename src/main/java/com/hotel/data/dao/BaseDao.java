package com.hotel.data.dao;

import java.util.List;

public interface BaseDao<T> {

    T create(T t);

    void create(List<T> list);

    T update(T t);

    List<T> update(List<T> list);

    T get(Long id);

    List<T> getAll();

    void delete(Long id);

}
