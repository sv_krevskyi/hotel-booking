package com.hotel.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hotel.data.entity.enums.RoomCategory;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode(of = "id")
@ToString
@Builder
@Entity
public class RoomType {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Setter(AccessLevel.NONE)
    private Long id;

    @Column
    @Enumerated(EnumType.STRING)
    private RoomCategory category;

    @Column
    private String description;

    @Column
    private Integer price;

//    @OneToMany(mappedBy = "roomType", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//    @JsonIgnore
//    private Set<Room> rooms = new HashSet<>();

//    @OneToMany(mappedBy = "roomType", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//    @JsonIgnore
//    private Set<RoomOrder> orders = new HashSet<>();
}
