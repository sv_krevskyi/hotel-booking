package com.hotel.data.dao.impl;

import com.hotel.Main;
import com.hotel.data.entity.Client;
import com.hotel.data.repository.ClientRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ActiveProfiles("test")
@SpringBootTest(classes = Main.class)
class ClientDaoMockTest {

    //expected
    private static final long TEST_ID = 1L;
    private static final Client TEST_CLIENT_EXPECTED = Client.builder()
            .firstName("Sasha")
            .lastName("Okhr")
            .id(TEST_ID)
            .build();
    private static final Client TEST_CLIENT = Client.builder()
            .firstName("Sasha")
            .lastName("Okhr").build();

    @Autowired
    private ClientDao clientDao;

    @MockBean
    private ClientRepository mockClientRepository;

    @Test
    void whenCreateThenReturnEntityTest() {
        clientDao.create(TEST_CLIENT);
        verify(mockClientRepository).save(TEST_CLIENT);
    }

    @Test
    void whenCreateListThenReturnEntityListTest() {
        clientDao.create(List.of(TEST_CLIENT));
        verify(mockClientRepository).saveAll(List.of(TEST_CLIENT));
    }

    @Test
    void whenGetThenReturnEntityTest() {
        when(mockClientRepository.findById(TEST_ID)).thenReturn(Optional.of(TEST_CLIENT_EXPECTED));
        Client actual = clientDao.get(TEST_ID);
        assertEquals(TEST_CLIENT_EXPECTED, actual);
    }

    @Test
    void whenGetAllThenReturnEntityListTest() {
        when(mockClientRepository.findAll()).thenReturn(List.of(TEST_CLIENT_EXPECTED));
        List<Client> actual = clientDao.getAll();
        assertEquals(List.of(TEST_CLIENT_EXPECTED), actual);
    }

    @Test
    void whenUpdateThenReturnEntityTest() {
        clientDao.update(TEST_CLIENT);
        verify(mockClientRepository).save(TEST_CLIENT);
    }

    @Test
    void whenUpdateListThenReturnEntityListTest() {
        clientDao.update(List.of(TEST_CLIENT));
        verify(mockClientRepository).saveAll(List.of(TEST_CLIENT));
    }

    @Test
    void deleteTest() {
        clientDao.delete(TEST_ID);
        verify(mockClientRepository).deleteById(TEST_ID);
    }

}