package com.hotel.data.repository;

import com.hotel.data.entity.RoomOrder;
import com.hotel.data.entity.enums.OrderStatus;
import org.springframework.data.repository.CrudRepository;

public interface RoomOrderRepository extends CrudRepository<RoomOrder, Long> {

    Iterable<RoomOrder> findAllByClientId(Long id);

    Iterable<RoomOrder> findAllByStatus(OrderStatus status);
}
