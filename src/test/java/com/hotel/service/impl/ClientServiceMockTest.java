package com.hotel.service.impl;

import com.hotel.data.dao.impl.ClientDao;
import com.hotel.data.dao.impl.RoomOrderDao;
import com.hotel.data.entity.Client;
import com.hotel.data.entity.Room;
import com.hotel.data.entity.RoomOrder;
import com.hotel.data.entity.RoomType;
import com.hotel.data.entity.enums.OrderStatus;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.sql.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ActiveProfiles("test")
@SpringBootTest
public class ClientServiceMockTest {

    private static final long TEST_ID = 1L;
    private static final Client TEST_CLIENT_EXPECTED = Client.builder()
            .firstName("Sasha")
            .lastName("Okhr")
            .id(TEST_ID)
            .build();

    private static final RoomType TEST_ROOM_TYPE = RoomType.builder().id(3L).build();
    private static final Room TEST_ROOM = Room.builder().id(4L).roomType(TEST_ROOM_TYPE).build();

    private static final RoomOrder TEST_ROOM_ORDER = RoomOrder.builder()
            .id(TEST_ID)
            .client(TEST_CLIENT_EXPECTED)
            .roomType(TEST_ROOM_TYPE)
            .room(TEST_ROOM)
            .checkInDate(Date.valueOf("2019-09-01"))
            .checkOutDate(Date.valueOf("2019-09-10"))
            .status(OrderStatus.WAITING)
            .build();

    @Autowired
    private ClientServiceImpl clientService;

    @MockBean
    private ClientDao clientDaoMock;

    @MockBean
    private RoomOrderDao roomOrdersDaoMock;

    @Test
    void whenAddClientShouldCreateTestClientTest() {
        //Given
        when(clientDaoMock.create(TEST_CLIENT_EXPECTED)).thenReturn(TEST_CLIENT_EXPECTED);

        //Then
        assertDoesNotThrow(() -> clientService.addClient(TEST_CLIENT_EXPECTED));
        verify(clientDaoMock).create(TEST_CLIENT_EXPECTED);
    }

    @Test
    void whenAddListOfClientsShouldCreateListOfTestClientTest() {
        //Given
        clientService.addClient(List.of(TEST_CLIENT_EXPECTED));

        //Then
        assertDoesNotThrow(() -> clientService.addClient(TEST_CLIENT_EXPECTED));
        verify(clientDaoMock).create(List.of(TEST_CLIENT_EXPECTED));
    }

    @Test
    void whenGetClientShouldReturnOneTest() {
        //Given
        when(clientDaoMock.get(TEST_ID)).thenReturn(TEST_CLIENT_EXPECTED);

        //When
        Client actual = clientService.getClient(TEST_ID);

        //Then
        assertEquals(TEST_CLIENT_EXPECTED, actual);
    }

    @Test
    void whenGetAllClientsShouldReturnListTest() {
        //Given
        when(clientDaoMock.getAll()).thenReturn(List.of(TEST_CLIENT_EXPECTED));

        //When
        List<Client> actual = clientService.getAllClients();

        //Then
        assertEquals(List.of(TEST_CLIENT_EXPECTED), actual);
    }

    @Test
    void whenChangeClientInfoShouldReturnTrueTest() {
        Client testClient = Client.builder()
                .firstName("TEST")
                .lastName("CLIENT")
                .id(TEST_ID)
                .build();
        when(clientDaoMock.get(TEST_ID)).thenReturn(testClient);
        clientService.changeClientInfo(TEST_ID, TEST_CLIENT_EXPECTED);
        //Then
        assertEquals(TEST_CLIENT_EXPECTED, testClient);
    }

    @Test
    void whenRemoveClientShouldReturnTrueTest() {
        clientService.removeClient(TEST_ID);
        verify(clientDaoMock).delete(TEST_ID);
    }

    @Test
    void whenGetClientOrdersShouldReturnListTest() {
        //Given
        when(roomOrdersDaoMock.getAllClientOrders(TEST_CLIENT_EXPECTED.getId())).thenReturn(List.of(TEST_ROOM_ORDER));

        //When
        List<RoomOrder> actual = clientService.getClientOrders(TEST_CLIENT_EXPECTED);

        //Then
        assertEquals(List.of(TEST_ROOM_ORDER), actual);
    }
}
