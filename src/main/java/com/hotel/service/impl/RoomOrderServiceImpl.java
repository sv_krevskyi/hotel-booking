package com.hotel.service.impl;

import com.hotel.data.dao.impl.ClientDao;
import com.hotel.data.dao.impl.RoomDao;
import com.hotel.data.dao.impl.RoomOrderDao;
import com.hotel.data.dao.impl.RoomTypeDao;
import com.hotel.data.entity.Client;
import com.hotel.data.entity.Room;
import com.hotel.data.entity.RoomOrder;
import com.hotel.data.entity.RoomType;
import com.hotel.data.entity.enums.OrderStatus;
import com.hotel.exception.IllegalActionException;
import com.hotel.service.RoomOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class RoomOrderServiceImpl implements RoomOrderService {

    private final RoomOrderDao roomOrderDao;
    private final RoomDao roomDao;
    private final ClientDao clientDao;
    private final RoomTypeDao roomTypeDao;

    @Autowired
    public RoomOrderServiceImpl(RoomOrderDao roomOrderDao, RoomDao roomDao, ClientDao clientDao, RoomTypeDao roomTypeDao) {
        this.roomOrderDao = roomOrderDao;
        this.roomDao = roomDao;
        this.clientDao = clientDao;
        this.roomTypeDao = roomTypeDao;
    }

    @Override
    public void placeOrderForClient(RoomOrder roomOrder, Long clientId, Long roomTypeId) {
        Client client = clientDao.get(clientId);
        RoomType roomType = roomTypeDao.get(roomTypeId);

//        roomType.getOrders().add(roomOrder);
        roomOrder.setRoomType(roomType);

        client.getOrders().add(roomOrder);
        roomOrder.setClient(client);
    }

    @Override
    public void placeOrderForClient(Long clientId, List<RoomOrder> roomOrders) {
//        for (RoomOrder roomOrder : roomOrders) {
//            placeOrderForClient(clientId, roomOrder);
//        }
    }

    @Override
    public RoomOrder getOrder(Long orderId) {
        return roomOrderDao.get(orderId);
    }

    @Override
    public List<RoomOrder> getAllRoomOrders() {
        return roomOrderDao.getAll();
    }

    @Override
    public List<RoomOrder> getRoomOrdersByStatus(String statusName) {
        OrderStatus status = OrderStatus.valueOf(statusName);
        return roomOrderDao.getAllByStatus(status);
    }

    @Override
    public List<RoomOrder> getRoomOrdersForClient(Long clientId) {
        return roomOrderDao.getAllClientOrders(clientId);
    }

    @Override
    public void approveOrder(RoomOrder order, Long roomId) {
        order.setRoom(roomDao.get(roomId));
        order.setStatus(OrderStatus.CONFIRMED);
        checkValidApprove(order);
        roomOrderDao.update(order);
    }

    @Override
    public boolean approveOrderIfRoomAvailable(Long id) {
        RoomOrder order = roomOrderDao.get(id);
        List<Room> availableRooms = roomDao.findAllAvailableRooms(order);
        if (availableRooms == null || availableRooms.isEmpty()) {
            return false;
        }
        approveOrder(order, availableRooms.get(0).getId());
        return true;
    }

    @Override
    public void updateStatus(Long id, String statusName) {
        updateStatus(Collections.singletonList(id), statusName);
    }

    @Override
    public void updateStatus(List<Long> idList, String statusName) {
        OrderStatus status = OrderStatus.valueOf(statusName);
        for (Long id : idList) {
            RoomOrder order = roomOrderDao.get(id);
            if (status == OrderStatus.CONFIRMED) checkValidApprove(order);
            order.setStatus(status);
        }
//        roomOrderDao.update(orders);
    }

    private void checkValidApprove(RoomOrder order) {
        if (order.getRoom().getId() == 0) {
            throw new IllegalActionException("Approved Order should have assigned Room");
        }
    }

}
