package com.hotel.data.dao.impl;

import com.hotel.data.dao.BaseDao;
import com.hotel.data.entity.RoomOrder;
import com.hotel.data.entity.enums.OrderStatus;
import com.hotel.data.repository.RoomOrderRepository;
import com.hotel.exception.DAOException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class RoomOrderDao implements BaseDao<RoomOrder> {

    private RoomOrderRepository roomOrderRepository;

    @Autowired
    public RoomOrderDao(RoomOrderRepository roomOrderRepository) {
        this.roomOrderRepository = roomOrderRepository;
    }

    public List<RoomOrder> getAllByStatus(OrderStatus status) {
        return (List<RoomOrder>) this.roomOrderRepository.findAllByStatus(status);
    }

    public List<RoomOrder> getAllClientOrders(Long clientId) {
        return (List<RoomOrder>) this.roomOrderRepository.findAllByClientId(clientId);
    }

    @Override
    public RoomOrder create(RoomOrder roomOrder) {
        return this.roomOrderRepository.save(roomOrder);
    }

    @Override
    public void create(List<RoomOrder> list) {
        this.roomOrderRepository.saveAll(list);
    }

    @Override
    public RoomOrder update(RoomOrder roomOrder) {
        return this.roomOrderRepository.save(roomOrder);
    }

    @Override
    public List<RoomOrder> update(List<RoomOrder> list) {
        return (List<RoomOrder>) this.roomOrderRepository.saveAll(list);
    }

    @Override
    public RoomOrder get(Long id) {
        return this.roomOrderRepository.findById(id).orElseThrow(()
                -> new DAOException(String.format("Order with id '%s' is not found", id)));
    }

    @Override
    public List<RoomOrder> getAll() {
        return (List<RoomOrder>) this.roomOrderRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        this.roomOrderRepository.deleteById(id);
    }

}
