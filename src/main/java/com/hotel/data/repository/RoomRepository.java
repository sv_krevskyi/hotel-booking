package com.hotel.data.repository;

import com.hotel.data.entity.Room;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.sql.Date;

public interface RoomRepository extends CrudRepository<Room, Long> {

    @Query(value = "SELECT Room.ID, Room.ROOM_TYPE_ID FROM Room " +
            "LEFT OUTER JOIN ROOM_ORDER ON Room.ID = ROOM_ORDER.ROOM_ID\n" +
            "WHERE Room.ROOM_TYPE_ID = ?1 AND Room.ID not in\n" +
            "(SELECT ROOM_ORDER.ROOM_ID FROM ROOM_ORDER WHERE\n" +
            "    ((?2 BETWEEN ROOM_ORDER.CHECK_IN_DATE AND ROOM_ORDER.CHECK_OUT_DATE) OR\n" +
            "      (?3 BETWEEN ROOM_ORDER.CHECK_IN_DATE AND ROOM_ORDER.CHECK_OUT_DATE) OR\n" +
            "      (?2 < ROOM_ORDER.CHECK_IN_DATE and ROOM_ORDER.CHECK_OUT_DATE < ?3))\n" +
            "           AND ROOM_ORDER.STATUS IN ('CONFIRMED', 'RESIDE'))", nativeQuery = true)
    Iterable<Room> findAllAvailableRooms(Long roomTypeId, Date checkInDate, Date checkOutDate);

}