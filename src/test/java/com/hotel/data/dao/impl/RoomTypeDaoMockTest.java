package com.hotel.data.dao.impl;

import com.hotel.data.entity.RoomType;
import com.hotel.data.repository.RoomTypeRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;

import static com.hotel.data.entity.enums.RoomCategory.SUPERIOR;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ActiveProfiles("test")
@SpringBootTest
class RoomTypeDaoMockTest {

    private static final long TEST_ID = 1L;

    private static final RoomType TEST_ROOM_TYPE_EXPECTED = RoomType.builder()
            .id(TEST_ID)
            .category(SUPERIOR)
            .description("room with double bad")
            .price(200)
            .build();

    private static final RoomType TEST_ROOM_TYPE = RoomType.builder()
            .category(SUPERIOR)
            .description("room with double bad")
            .price(200)
            .build();

    @Autowired
    private RoomTypeDao roomTypeDao;

    @MockBean
    private RoomTypeRepository mockRoomTypeRepository;

    @Test
    void whenCreateThenReturnEntityTest() {
        roomTypeDao.create(TEST_ROOM_TYPE);
        verify(mockRoomTypeRepository).save(TEST_ROOM_TYPE);
    }

    @Test
    void whenCreateListThenReturnEntityListTest() {
        roomTypeDao.create(List.of(TEST_ROOM_TYPE));
        verify(mockRoomTypeRepository).saveAll(List.of(TEST_ROOM_TYPE));
    }

    @Test
    void whenGetThenReturnEntityTest() {
        when(mockRoomTypeRepository.findById(TEST_ID)).thenReturn(Optional.of(TEST_ROOM_TYPE_EXPECTED));
        RoomType actual = roomTypeDao.get(TEST_ID);
        assertEquals(TEST_ROOM_TYPE_EXPECTED, actual);
    }

    @Test
    void whenGetAllThenReturnEntityListTest() {
        when(mockRoomTypeRepository.findAll()).thenReturn(List.of(TEST_ROOM_TYPE_EXPECTED));
        List<RoomType> actual = roomTypeDao.getAll();
        assertEquals(List.of(TEST_ROOM_TYPE_EXPECTED), actual);
    }

    @Test
    void whenUpdateThenReturnEntityTest() {
        roomTypeDao.update(TEST_ROOM_TYPE);
        verify(mockRoomTypeRepository).save(TEST_ROOM_TYPE);
    }

    @Test
    void whenUpdateListThenReturnEntityListTest() {
        roomTypeDao.update(List.of(TEST_ROOM_TYPE));
        verify(mockRoomTypeRepository).saveAll(List.of(TEST_ROOM_TYPE));
    }

    @Test
    void deleteTest() {
        roomTypeDao.delete(TEST_ID);
        verify(mockRoomTypeRepository).deleteById(TEST_ID);
    }
}