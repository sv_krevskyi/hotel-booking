package com.hotel.data.dao.impl;

import com.hotel.data.dao.BaseDao;
import com.hotel.data.entity.Room;
import com.hotel.data.entity.RoomOrder;
import com.hotel.data.repository.RoomRepository;
import com.hotel.exception.DAOException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class RoomDao implements BaseDao<Room> {

    private RoomRepository roomRepository;

    @Autowired
    public RoomDao(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    public List<Room> findAllAvailableRooms(RoomOrder roomOrder) {
        return (List<Room>) this.roomRepository
                .findAllAvailableRooms(roomOrder.getRoomType().getId(),
                        roomOrder.getCheckInDate(),
                        roomOrder.getCheckOutDate());
    }

    @Override
    public Room create(Room room) {
        return this.roomRepository.save(room);
    }

    @Override
    public void create(List<Room> list) {
        this.roomRepository.saveAll(list);
    }

    @Override
    public Room update(Room room) {
        return this.roomRepository.save(room);
    }

    @Override
    public List<Room> update(List<Room> list) {
        return (List<Room>) this.roomRepository.saveAll(list);
    }

    @Override
    public Room get(Long id) {
        return this.roomRepository.findById(id).orElseThrow(()
                -> new DAOException(String.format("Room with id '%s' is not found", id)));
    }

    @Override
    public List<Room> getAll() {
        return (List<Room>) this.roomRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        this.roomRepository.deleteById(id);
    }

}
